import { LUA_CHON, PLAY_GAME, TAI, XIU } from "../../constant/constant.js";
const initialState = {
  luaChon: "",
  soBanThang: 0,
  soLuotChoi: 0,
  score: 0,
  mangXucXac: [
    {
      img: "./xuc_xac/1.png",
      giaTri: 1,
    },
    {
      img: "./xuc_xac/1.png",
      giaTri: 1,
    },
    {
      img: "./xuc_xac/1.png",
      giaTri: 1,
    },
  ],
};
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      console.log("case game");
      state.soLuotChoi++;
      let score = 0;
      let newMangXucXac = [];
      state.mangXucXac.forEach((item) => {
        let randomNum = Math.floor(Math.random() * 6 + 1);
        score += randomNum;
        let newXucXac = {
          img: `./xuc_xac/${randomNum}.png`,
          giaTri: randomNum,
        };
        newMangXucXac.push(newXucXac);
      });
      score >= 11 && state.luaChon == TAI && state.soBanThang++;
      score < 11 && state.luaChon == XIU && state.soBanThang++;

      state.mangXucXac = newMangXucXac;
      state.score = score;
      console.log("score", score);
      return { ...state };
    }
    case LUA_CHON: {
      state.luaChon = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
