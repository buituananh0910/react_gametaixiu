import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME, TAI, XIU } from "./constant/constant.js";
export class KetQua extends Component {
  render() {
    return (
      <div className="text-center pt-5 display-4">
        <button
          className="btn btn-success"
          onClick={() => this.props.handlePlayGame(this.props.luaChon)}
        >
          <span className="display-4">Play Game</span>
        </button>
        {this.props.luaChon && (
          <p
            className={this.props.luaChon == TAI ? "text-danger" : "text-dark"}
          >
            Bạn chọn : {this.props.luaChon}
          </p>
        )}
        <p>Số bàn thắng : {this.props.soBanThang}</p>
        <p>Số bàn chơi : {this.props.soLuotChoi}</p>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  luaChon: state.xucXacReducer.luaChon,
  soBanThang: state.xucXacReducer.soBanThang,
  soLuotChoi: state.xucXacReducer.soLuotChoi,
});

const mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: (luaChon) => {
      if (luaChon !== "") {
        dispatch({
          type: PLAY_GAME,
        });
      } else {
        alert("Bạn Chưa Chọn Tài Hoặc Xỉu");
      }
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
