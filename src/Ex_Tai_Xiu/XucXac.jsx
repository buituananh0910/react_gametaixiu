import React, { Component } from "react";
import { connect } from "react-redux";
import { LUA_CHON, TAI, XIU } from "./constant/constant";
import img1 from "./video_19_game_xuc_xac/1.png";
import img2 from "./video_19_game_xuc_xac/2.png";
import img3 from "./video_19_game_xuc_xac/3.png";
import img4 from "./video_19_game_xuc_xac/4.png";
import img5 from "./video_19_game_xuc_xac/5.png";
import img6 from "./video_19_game_xuc_xac/6.png";

let btnStyle = {
  width: 150,
  height: 150,
  fontSize: 44,
};
export class XucXac extends Component {
  render() {
    let { mangXucXac } = this.props;
    let kq = this.props.score >= 11 ? TAI : XIU;
    return (
      <div className="container pt-5">
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <button
            style={btnStyle}
            className="btn btn-danger"
            onClick={() => this.props.handleSelectOption(TAI)}
          >
            Tài
          </button>
          <div className="mangXucXac">
            {/* render mảng xúc xắc */}
            {mangXucXac.map((item) => {
              let { giaTri } = item;
              let classAnimation = "animation" + giaTri;
              return (
                <div className="xucXac">
                  <div className={classAnimation}>
                    <img style={{ width: 100 }} src={img1} alt="" />
                    <img style={{ width: 100 }} src={img2} alt="" />
                    <img style={{ width: 100 }} src={img3} alt="" />
                    <img style={{ width: 100 }} src={img4} alt="" />
                    <img style={{ width: 100 }} src={img5} alt="" />
                    <img style={{ width: 100 }} src={img6} alt="" />
                  </div>
                </div>
              );
            })}
          </div>

          <button
            style={btnStyle}
            className="btn btn-secondary"
            onClick={() => this.props.handleSelectOption(XIU)}
          >
            Xỉu
          </button>
        </div>
        <p
          style={{
            fontSize: "40px",
            marginTop: "20px",
            color: "yellow",
            fontWeight: "bolder",
          }}
        >
          Kết Quả : {this.props.score} {kq}
        </p>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  mangXucXac: state.xucXacReducer.mangXucXac,
  score: state.xucXacReducer.score,
});

const mapDispatchToProps = (dispatch) => {
  return {
    handleSelectOption: (luaChon) => {
      dispatch({
        type: LUA_CHON,
        payload: luaChon,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucXac);
